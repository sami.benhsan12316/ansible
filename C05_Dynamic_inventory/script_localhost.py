#!/usr/bin/env python3
import json

def generate_dynamic_inventory():
    # Simulated localhost data
    localhost_data = {
        "name": "Sémi ben hssan",
        "ip_address": "127.0.0.1",
        "local_variable": "value1",
        "other_variable": "value2"
    }

    # Create an inventory structure
    inventory = {
        "localhost": {
            "hosts": [localhost_data["name"]],
            "vars": {
                "ansible_connection": "local"
            }
        },
        "_meta": {
            "hostvars": {localhost_data["name"]: localhost_data}
        }
    }

    print(json.dumps(inventory, indent=4))

if __name__ == "__main__":
    generate_dynamic_inventory()
