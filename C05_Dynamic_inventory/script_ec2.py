#!/usr/bin/env python3
import json
import boto3

def get_ec2_instances():
    ec2 = boto3.client('ec2', region_name='us-east-1')
    response = ec2.describe_instances()
    instances = []

    for reservation in response['Reservations']:
        for instance in reservation['Instances']:
            instances.append({
                'name': instance['Tags'][0]['Value'],  # Assuming the first tag is the instance name
                'private_ip': instance['PrivateIpAddress'],
                'public_ip': instance.get('PublicIpAddress', 'N/A')
            })

    return instances

def generate_dynamic_inventory():
    ec2_instances = get_ec2_instances()
    inventory = {
        'ec2': {
            'hosts': [instance['name'] for instance in ec2_instances],
            'vars': {
                'ansible_user': 'ec2-user',
                'ansible_ssh_private_key_file': 'path/to/your/key.pem'
            }
        },
        '_meta': {
            'hostvars': {instance['name']: instance for instance in ec2_instances}
        }
    }

    print(json.dumps(inventory))
    
if __name__ == '__main__':
    generate_dynamic_inventory()